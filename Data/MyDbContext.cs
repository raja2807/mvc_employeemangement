using mvc_employeemangement.Models;
using Microsoft.EntityFrameworkCore;



namespace mvc_employeemangement.Models
{
    public class MyDbContext : DbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
        {
        }

        // DbSet for the Employee entity
        public DbSet<Employee> Employees { get; set; }

        // Add DbSet for other entities if needed

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Configure your model if needed
            // modelBuilder.Entity<YourEntity>().ToTable("YourTableName");
            // modelBuilder.Entity<YourEntity>().HasKey(e => e.Id);
            // ...

            base.OnModelCreating(modelBuilder);
        }
    }
}
