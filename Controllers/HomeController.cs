﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using mvc_employeemangement.Models;
using System.Diagnostics;
using System.Linq;

public class HomeController : Controller
{
    private readonly MyDbContext _dbContext;
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger, MyDbContext dbContext)
    {
        _logger = logger;
        _dbContext = dbContext;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    
    public IActionResult Login()
    {
        return View();
    }

    [HttpPost]
    public IActionResult Login(Employee model)
    {
        if (ModelState.IsValid)
        {
            var user = _dbContext.Employees.FirstOrDefault(e => e.UserName == model.UserName && e.Password == model.Password);

            if (user != null)
            {
                return RedirectToAction("Logindone");
            }

            ModelState.AddModelError(string.Empty, "Invalid login attempt. Please check your username and password.");
        }

        return View(model);
    }

    public IActionResult Logindone()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
